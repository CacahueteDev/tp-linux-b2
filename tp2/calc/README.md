# Build le container

```shell
docker build -t calc .
```

# Lancer l'app

## Directement

```shell
$ docker run -e CALC_PORT={port} calc
```

## Avec Docker Compose

```shell
$ docker compose up
```