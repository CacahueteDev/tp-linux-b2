# TP1

# I. Init

## 3. sudo c pa bo

### Ajouter votre utilisateur au groupe docker

```shell
[rocky@server ~]$ sudo usermod -aG docker rocky
[rocky@server ~]$ exit

[...]

Activate the web console with: systemctl enable --now cockpit.socket

Last login: Fri Dec 22 10:50:25 2023 from 192.168.56.118
[rocky@server ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

### Lancer un conteneur NGINX

```shell
[rocky@server ~]$ docker run -d -p 9999:80 nginx

Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
af107e978371: Pull complete
336ba1f05c3e: Pull complete
8c37d2ff6efa: Pull complete
51d6357098de: Pull complete
782f1ecce57d: Pull complete
5e99d351b073: Pull complete
7b73345df136: Pull complete
Digest: sha256:2bdc49f2f8ae8d8dc50ed00f2ee56d00385c6f8bc8a8b320d0a294d9e3b49026
Status: Downloaded newer image for nginx:latest
3d39c5577fbf6ac0bf5da4d537bb6e15a839f129abc262d7e94487b286c5ada6
```

### Visitons

```shell
[rocky@server ~]$ docker ps

CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
3d39c5577fbf   nginx     "/docker-entrypoint.…"   21 minutes ago   Up 21 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp   vibrant_zhukovsky
```
```
[rocky@server ~]$ docker logs vibrant_zhukovsky

/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/22 12:58:32 [notice] 1#1: using the "epoll" event method
2023/12/22 12:58:32 [notice] 1#1: nginx/1.25.3
2023/12/22 12:58:32 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/12/22 12:58:32 [notice] 1#1: OS: Linux 5.14.0-284.30.1.el9_2.x86_64
2023/12/22 12:58:32 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/22 12:58:32 [notice] 1#1: start worker processes
2023/12/22 12:58:32 [notice] 1#1: start worker process 28
```
```
[rocky@server ~]$ docker inspect vibrant_zhukovsky

[
    {
        "Id": "3d39c5577fbf6ac0bf5da4d537bb6e15a839f129abc262d7e94487b286c5ada6",
        "Created": "2023-12-22T12:58:32.575489002Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 1919,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-22T12:58:32.894727457Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        }
        [...]
    }
]
```
```
[rocky@server ~]$ sudo ss -lnpt

[sudo] password for rocky:
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port     Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*         users:(("sshd",pid=721,fd=3))
LISTEN     0          4096                 0.0.0.0:9999              0.0.0.0:*         users:(("docker-proxy",pid=1874,fd=4))
LISTEN     0          128                     [::]:22                   [::]:*         users:(("sshd",pid=721,fd=4))
LISTEN     0          4096                    [::]:9999                 [::]:*         users:(("docker-proxy",pid=1881,fd=4))
```
```
[rocky@server ~]$ sudo firewall-cmd --permanent --add-port=9999/tcp

success
```

### On va ajouter un site Web au conteneur NGINX

```shell
[rocky@server ~]$ mkdir nginx
[rocky@server ~]$ cd nginx/
[rocky@server nginx]$ nano index.html
[rocky@server nginx]$ nano site_nul.conf
[rocky@server nginx]$ docker run -d -p 9999:8080 -v /home/rocky/nginx/index.html:/var/www/html/index.html -v /home/rocky/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
00a13d8932bf7527dfcf8c58ff463acf7e90ac0f80a61eaed9416776cdd3d8ab
[rocky@server nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                               NAMES
00a13d8932bf   nginx     "/docker-entrypoint.…"   15 seconds ago   Up 14 seconds   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   jovial_elion
```

### Visitons

C'est OK

## 5. Un deuxième conteneur en vif

### Lance un conteneur Python, avec un shell

```shell
[rocky@server nginx]$ docker run -it python bash
Unable to find image 'python:latest' locally
latest: Pulling from library/python
bc0734b949dc: Pull complete
b5de22c0f5cd: Pull complete
917ee5330e73: Pull complete
b43bd898d5fb: Pull complete
7fad4bffde24: Pull complete
d685eb68699f: Pull complete
107007f161d0: Pull complete
02b85463d724: Pull complete
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Downloaded newer image for python:latest
root@c1fd76490e1e:/# hello
bash: hello: command not found
root@c1fd76490e1e:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()
root@c1fd76490e1e:/#
```

### Installe des libs Python

```shell
root@c1fd76490e1e:/# pip install aiohttp
[...]
root@c1fd76490e1e:/# pip install aioconsole
[...]
root@c1fd76490e1e:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
>>> exit()
```

# II. Images

## 1. Images publiques

### Récupérer des images

```shell
[rocky@server ~]$ docker pull python
Using default tag: latest
latest: Pulling from library/python
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Image is up to date for python:latest
docker.io/library/python:latest
```
```
[rocky@server ~]$ docker pull mysql
Using default tag: latest
latest: Pulling from library/mysql
bce031bc522d: Pull complete
cf7e9f463619: Pull complete
105f403783c7: Pull complete
878e53a613d8: Pull complete
2a362044e79f: Pull complete
6e4df4f73cfe: Pull complete
69263d634755: Pull complete
fe5e85549202: Pull complete
5c02229ce6f1: Pull complete
7320aa32bf42: Pull complete
Digest: sha256:4ef30b2c11a3366d7bb9ad95c70c0782ae435df52d046553ed931621ea36ffa5
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest
```
```
[rocky@server ~]$ docker pull wordpress:latest
latest: Pulling from library/wordpress
af107e978371: Already exists
6480d4ad61d2: Pull complete
95f5176ece8b: Pull complete
0ebe7ec824ca: Pull complete
673e01769ec9: Pull complete
74f0c50b3097: Pull complete
1a19a72eb529: Pull complete
50436df89cfb: Pull complete
8b616b90f7e6: Pull complete
df9d2e4043f8: Pull complete
d6236f3e94a1: Pull complete
59fa8b76a6b3: Pull complete
99eb3419cf60: Pull complete
22f5c20b545d: Pull complete
1f0d2c1603d0: Pull complete
4624824acfea: Pull complete
79c3af11cab5: Pull complete
e8d8239610fb: Pull complete
a1ff013e1d94: Pull complete
31076364071c: Pull complete
87728bbad961: Pull complete
Digest: sha256:ffabdfe91eefc08f9675fe0e0073b2ebffa8a62264358820bcf7319b6dc09611
Status: Downloaded newer image for wordpress:latest
docker.io/library/wordpress:latest
```
```
[rocky@server ~]$ docker pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
8b16ab80b9bd: Pull complete
07a0e16f7be1: Pull complete
145cda5894de: Pull complete
1a16fa4f6192: Pull complete
84d558be1106: Pull complete
4573be43bb06: Pull complete
20b23561c7ea: Pull complete
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest
[rocky@server ~]$
```

### Lancez un conteneur à partir de l'image Python

```shell
[rocky@server ~]$ docker run -it python:3.11 bash
root@d2b451f48538:/# python --version
Python 3.11.7
```

## 2. Construire une image

### Build l'image

```shell
[rocky@server dod]$ docker build . -t papp:turfu
[+] Building 0.3s (10/10) FINISHED                                                                                                            docker:default
 => [internal] load build definition from Dockerfile                                                                                                    0.0s
 => => transferring dockerfile: 270B                                                                                                                    0.0s
 => [internal] load .dockerignore                                                                                                                       0.0s
 => => transferring context: 2B                                                                                                                         0.0s
 => [internal] load metadata for docker.io/library/debian:latest                                                                                        0.0s
 => [1/7] FROM docker.io/library/debian                                                                                                                 0.0s
 => [internal] load build context                                                                                                                       0.0s
 => => transferring context: 86B                                                                                                                        0.0s
 => CACHED [2/7] RUN apt update                                                                                                                         0.0s
 => CACHED [3/7] RUN apt upgrade -y                                                                                                                     0.0s
 => CACHED [4/7] RUN apt install -y python3 python3-emoji                                                                                               0.0s
 => CACHED [5/7] RUN mkdir /app                                                                                                                         0.0s
 => CACHED [6/7] COPY app.py /app/app.py                                                                                                                0.0s
 => [7/7] WORKDIR /app                                                                                                                                  0.0s
 => exporting to image                                                                                                                                  0.0s
 => => exporting layers                                                                                                                                 0.0s
 => => writing image sha256:aeb552b2a64ebd9839807b4de431bd1fd7df95379bdf580abdfbc33f0cce4f9e                                                            0.0s
 => => naming to docker.io/library/papp:turfu
```

### Lancer l'image

```
[rocky@server dod]$ docker run papp:turfu
Cet exemple d'application est vraiment naze 👎
```

# III. Docker Compose

## Lancez les deux conteneurs avec docker compose

```shell
[rocky@server compose_test]$ docker compose up -d
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                                                     0.2s
 ✔ Container compose_test-conteneur_flopesque-1  Started                                                                     0.0s
 ✔ Container compose_test-conteneur_nul-1        Started
```

## Vérifier que les deux conteneurs tournent

```shell
[rocky@server compose_test]$ docker ps
CONTAINER ID   IMAGE     COMMAND        CREATED         STATUS         PORTS     NAMES
f2767c271f74   debian    "sleep 9999"   3 minutes ago   Up 3 minutes             compose_test-conteneur_nul-1
c3b8eca11ff4   debian    "sleep 9999"   3 minutes ago   Up 3 minutes
```

## Pop un shell dans le conteneur conteneur_nul

